# Docker image for Ansible + Packer based on Linux Alpine

Run Ansible + Packer on a light Linux distribution. Especially useful for CI environments such as Gitlab CI.

## Versions

Current image includes:

- Alpine 3.11
- Ansible 2.9.5
- Packer 1.5.4

## Examples

To use in a GitLab CI `.gitlab-ci.yml`:

```yml
image:
  name: iromain/packer-ansible-alpine:latest
build:
  script:
    - ansible --version
    - packer --version
```

See a [real life example](https://gitlab.com/iRomain/packer-aws-mongodb-ami/-/blob/master/.gitlab-ci.yml) in my other repo building a MongoDB AMI using this image.

## Modifying the image

Update the `Dockerfile` and run `docker build .`
